﻿using Estruturacao.Domain.EntitiesInOut.DTO.Categoria;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria;

namespace Estruturacao.Domain.Interfaces.Adapters
{
    public interface IAplicacaoCategoria
    {
        void Adicionar(CategoriaDTO request);
        void Alterar(int id, CategoriaDTO request);
        IEnumerable<CategoriaViewModel> Listar();
        CategoriaViewModel? Selecionar(int id);
        void Deletar(int id);
        IEnumerable<CategoriaQuantidadeVendidaViewModel> ListarPorQuantidadeVendida();
    }
}
