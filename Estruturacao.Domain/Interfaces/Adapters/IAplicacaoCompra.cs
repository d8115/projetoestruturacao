﻿using Estruturacao.Domain.EntitiesInOut.DTO.Compra;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Compra;

namespace Estruturacao.Domain.Interfaces.Adapters
{
    public interface IAplicacaoCompra
    {
        void Comprar(CompraDTO compraDTO);
        IEnumerable<CompraViewModel> Listar(int idProduto, int idCliente, string? cpfCliente);
    }
}
