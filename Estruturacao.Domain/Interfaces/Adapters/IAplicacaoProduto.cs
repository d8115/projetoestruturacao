﻿using Estruturacao.Domain.EntitiesInOut.DTO.Produto;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Produto;

namespace Estruturacao.Domain.Adapters
{
    public interface IAplicacaoProduto
    {
        void Adicionar(ProdutoDTO request);
        void Alterar(ProdutoDTO request, int id);
        IList<ProdutoViewModel> Listar(bool somenteEstoque, int idCategoria, bool semCategoria);
        ProdutoViewModel? Selecionar(int id);
        void Deletar(int id);
        void Desativar(int id);
        IList<ProdutoViewModel> ListarPorMaisVendido();
    }
}
