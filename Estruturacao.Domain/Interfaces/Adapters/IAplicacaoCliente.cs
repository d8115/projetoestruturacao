﻿using Estruturacao.CrossCutting.CEPServices.Entities;
using Estruturacao.Domain.EntitiesInOut.In;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Cliente;

namespace Estruturacao.Domain.Adapters
{
    public interface IAplicacaoCliente
    {
        void Adicionar(ClienteDTO request);
        void Alterar(ClienteDTO request, int id);
        IEnumerable<ClienteViewModel> Listar();
        ClienteViewModel? Selecionar(string? cpf, int id);
        void Deletar(int id);
        IEnumerable<ClienteViewModel> ListarClienteOrdenadoPorGasto();
        IEnumerable<ClienteViewModel> ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto();
        IEnumerable<ClienteViewModel> ListarViewModelsordenadoPorQuantidadeProdutosComprados();
        InformacoesCEP? ExibirInformacoesCEPPorIdCliente(int idCliente);
    }
}
