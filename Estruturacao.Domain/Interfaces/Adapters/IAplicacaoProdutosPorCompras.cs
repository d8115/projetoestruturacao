﻿using Estruturacao.Domain.Entities;

namespace Estruturacao.Domain.Interfaces.Adapters
{
    public interface IAplicacaoProdutosPorCompras
    {
        IList<ProdutosPorCompra> Listar();
    }
}
