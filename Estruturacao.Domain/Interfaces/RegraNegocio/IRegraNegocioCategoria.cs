﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Categoria;

namespace Estruturacao.Domain.Interfaces.RegraNegocio
{
    public interface IRegraNegocioCategoria
    {
        void Incluir(CategoriaDTO categoriaDTO);
        Categoria Validar(int id, bool delete = false);
    }
}
