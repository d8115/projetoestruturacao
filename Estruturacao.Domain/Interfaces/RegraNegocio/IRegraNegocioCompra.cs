﻿using Estruturacao.Domain.EntitiesInOut.DTO.Compra;

namespace Estruturacao.Domain.Interfaces.RegraNegocio
{
    public interface IRegraNegocioCompra
    {
        int Incluir(CompraDTO compraDTO);
    }
}
