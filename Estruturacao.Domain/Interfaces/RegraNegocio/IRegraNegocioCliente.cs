﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.In;

namespace Estruturacao.Domain.Interfaces.RegraNegocio
{
    public interface IRegraNegocioCliente
    {
        void IncluirLista(IList<ClienteDTO> lista);
        Cliente Validar(int id, bool delete = false);
    }
}
