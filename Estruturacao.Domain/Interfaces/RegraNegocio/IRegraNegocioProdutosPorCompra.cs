﻿using Estruturacao.Domain.Entities;

namespace Estruturacao.Domain.Interfaces.RegraNegocio
{
    public interface IRegraNegocioProdutosPorCompra
    {
        void Incluir(ProdutosPorCompra produtosPorCompra);
    }
}
