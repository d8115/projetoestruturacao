﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Produto;

namespace Estruturacao.Domain.Interfaces.RegraNegocio
{
    public interface IRegraNegocioProduto
    {
        void Incluir(ProdutoDTO produtoDTO);
        Produto Validar(int id, bool alteracao, ProdutoDTO? produtoDTO);
    }
}
