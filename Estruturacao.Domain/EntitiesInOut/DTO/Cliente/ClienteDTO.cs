﻿namespace Estruturacao.Domain.EntitiesInOut.In
{
    public class ClienteDTO
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public DateTime Data_Nascimento { get; set; }
        public string Email { get; set; }
        public string? CEP { get; set; }
    }
}
