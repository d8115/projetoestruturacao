﻿namespace Estruturacao.Domain.EntitiesInOut.DTO.Compra
{
    public class CompraDTO
    {
        public int IdComprador { get; set; }
        public List<ProdutoComprado> ProdutosComprados { get; set; }

        public class ProdutoComprado
        {
            public int IdProduto { get; set; }
            public int Quantidade { get; set; }
        }
    }
}
