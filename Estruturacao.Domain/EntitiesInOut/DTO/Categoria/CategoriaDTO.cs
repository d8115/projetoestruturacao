﻿namespace Estruturacao.Domain.EntitiesInOut.DTO.Categoria
{
    public class CategoriaDTO
    {
        public string Nome { get; set; }
    }
}
