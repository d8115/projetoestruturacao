﻿namespace Estruturacao.Domain.EntitiesInOut.DTO.Produto
{
    public class ProdutoDTO
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int? CategoriaId { get; set; }
    }
}
