﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.Cliente
{
    public class ClienteViewModel
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public DateTime? Data_Nascimento { get; set; }
        public string? Email { get; set; }
        public string? CEP { get; set; }
        public int? QuantidadeCompras { get; set; }

        public DateTime Modificacao { get; set; }
    }
}
