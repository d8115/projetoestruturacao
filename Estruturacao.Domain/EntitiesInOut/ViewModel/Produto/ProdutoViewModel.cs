﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.Produto
{
    public class ProdutoViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int? CategoriaId { get; set; }
        public decimal? QuantidadeVendida { get; set; }
        public decimal? VendaUnitaria { get; set; }

        public DateTime Modificacao { get; set; }
        public bool? EmEstoque { get; set; }
    }
}
