﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.Compra
{
    public class CompraViewModel
    {
        public int IdComprador { get; set; }
        public int IdCompra { get; set; }
        public decimal QuantidadeProdutos { get; set; }
        public DateTime DataCompra { get; set; }
    }
}
