﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria
{
    public class CategoriaQuantidadeVendidaViewModel
    {
        public string NomedaCategoria { get; set; }
        public decimal QuantidadeVendida { get; set; }
    }
}
