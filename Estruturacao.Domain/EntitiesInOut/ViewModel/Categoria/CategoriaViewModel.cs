﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria
{
    public class CategoriaViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal ProdutosVendidosComACategoria { get; set; }

        public DateTime Modificacao { get; set; }
    }
}
