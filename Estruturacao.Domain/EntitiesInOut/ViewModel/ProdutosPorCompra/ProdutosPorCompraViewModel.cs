﻿namespace Estruturacao.Domain.EntitiesInOut.ViewModel.ProdutosPorCompra
{
    public class ProdutosPorCompraViewModel
    {
        public int Id { get; set; }
        public int ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public int CompraId { get; set; }
        public DateTime Modificacao { get; set; }
    }
}
