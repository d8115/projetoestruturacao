﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.Entidades.Base;

namespace Estruturacao.Domain.Entities
{
    public class ProdutosPorCompra : EntidadeBase
    {
        public int ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public int CompraId { get; set; }

        public virtual Produto Produto { get; set; }
        public virtual Compra Compra { get; set; }
    }
}
