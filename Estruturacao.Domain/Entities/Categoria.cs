﻿using Estruturacao.Domain.Entidades.Base;

namespace Estruturacao.Domain.Entidades
{
    public class Categoria : EntidadeBase
    {
        public string Nome { get; set; }

        public List<Produto>? Produto { get; set; }
    }
}