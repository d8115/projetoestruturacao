﻿namespace Estruturacao.Domain.Entidades.Base
{
    public class EntidadeBase
    {
        public virtual int Id { get; set; }
        public DateTime Modificacao { get; set; }
    }
}
