﻿using Estruturacao.Domain.Entidades.Base;

namespace Estruturacao.Domain.Entidades
{
    public class Cliente : EntidadeBase
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public DateTime? Data_Nascimento { get; set; }
        public string? Email { get; set; }
        public string? CEP { get; set; }

        public virtual List<Compra>? Compras { get; set; }
    }
}
