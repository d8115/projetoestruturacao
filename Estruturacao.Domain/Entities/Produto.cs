﻿using Estruturacao.Domain.Entidades.Base;
using Estruturacao.Domain.Entities;

namespace Estruturacao.Domain.Entidades
{
    public class Produto : EntidadeBase
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int? CategoriaId { get; set; }
        public bool? Status { get; set; }

        public virtual Categoria? Categoria { get; set; }
        public virtual List<ProdutosPorCompra> ProdutosProCompra { get; set; }
    }
}