﻿using Estruturacao.Domain.Entidades.Base;
using Estruturacao.Domain.Entities;

namespace Estruturacao.Domain.Entidades
{
    public class Compra : EntidadeBase
    {
        public int ClienteId { get; set; }

        public virtual Cliente? Cliente { get; set; }
        public virtual List<ProdutosPorCompra>? ProdutosCompra { get; set; }
    }
}