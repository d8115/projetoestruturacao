﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estruturacao.CrossCutting.FileServices
{
    public static class FileCheck
    {
        public static bool IsOpened(string path)
        {
            try
            {
                var file = new StreamWriter(path);
                file.Close();
                return false;
            }
            catch
            {
                return true;
            }
        }
    }
}
