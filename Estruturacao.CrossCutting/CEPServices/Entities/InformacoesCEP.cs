﻿using Newtonsoft.Json;

namespace Estruturacao.CrossCutting.CEPServices.Entities
{
    public class InformacoesCEP
    {
        [JsonProperty("cep")]
        public string CEP { get; set; }
        [JsonProperty("logradouro")]
        public string Logradouro { get; set; }
        [JsonProperty("bairro")]
        public string Bairro { get; set; }
        [JsonProperty("localidade")]
        public string Localidade { get; set; }
        [JsonProperty("uf")]
        public string UF { get; set; }
        [JsonProperty("ddd")]
        public string DDD { get; set; }
        [JsonProperty("complemento")]
        public string Complemento { get; set; }
    }
}
