﻿using Estruturacao.CrossCutting.CEPServices.Entities;
using Newtonsoft.Json;

namespace Estruturacao.CrossCutting.CEPServices
{
    public static class BuscarInformacoesCEP
    {
        public static InformacoesCEP? Buscar(string? cep)
        {
            HttpClient client = new();

            var response = client.GetAsync($"https://viacep.com.br/ws/{cep}/json/").Result;

            if (!response.IsSuccessStatusCode)
                throw new Exception("CEP não encontrado");

            var content = response.Content.ReadAsStringAsync().Result;
            var retorno = JsonConvert.DeserializeObject<InformacoesCEP>(content);

            return retorno;
        }
    }
}
