﻿using System.Globalization;
using CsvHelper;
using Estruturacao.CrossCutting.FileServices;

namespace Estruturacao.CrossCutting.CSVServices
{
    public static class SalvarCSV
    {
        public static void Salvar<T>(IList<T> list, string path)
        {
            if (FileCheck.IsOpened(path))
                throw new Exception("Arquivo em uso no momento");

            using (var file = new StreamWriter(path))
            using (var csv = new CsvWriter(file, CultureInfo.InvariantCulture))
               csv.WriteRecords(list);
        }
    }
}
