﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estruturacao.CrossCutting.CSVServices
{
    public static class LerCSV
    {
        public async static Task<IEnumerable<T>> Ler<T>(string path)
        {
            using (var stream = new StreamReader(path))
            using (var reader = new CsvHelper.CsvReader(stream, CultureInfo.InvariantCulture))
                return await Task.FromResult(reader.GetRecords<T>().ToList());
        }
    }
}
