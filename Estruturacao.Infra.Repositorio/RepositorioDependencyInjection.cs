﻿using Estruturacao.Infra.Repositorio.Repositories.CategoriaRepository;
using Estruturacao.Infra.Repositorio.Repositories.ClienteRepository;
using Estruturacao.Infra.Repositorio.Repositories.CompraRepository;
using Estruturacao.Infra.Repositorio.Repositories.ProdutoRepository;
using Estruturacao.Infra.Repositorio.Repositories.ProdutosPorCompraRepository;
using Microsoft.Extensions.DependencyInjection;

namespace Estruturacao.Infra.Repositorio
{
    public static class RepositorioDependencyInjection
    {
        public static void AddRepositorioDependency(this IServiceCollection service)
        {
            service.AddTransient<IClienteRepository, ClienteRepository>();
            service.AddTransient<ICategoriaRepository, CategoriaRepository>();
            service.AddTransient<IProdutoRepository, ProdutoRepository>();
            service.AddTransient<ICompraRepository, CompraRepository>();
            service.AddTransient<IProdutosPorCompraRepository, ProdutosPorCompraRepository>();
        }
    }
}
