﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Produto;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.ProdutoRepository
{
    public class ProdutoRepository : BaseRepository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(AppDbContext context) : base(context) { }

        public ProdutoViewModel? SelecionarViewModel(int id)
        {
            var query = from p in _context.Produtos
                        join pc in _context.ProdutosPorCompras on p.Id equals pc.ProdutoId into j1
                        from j2 in j1.DefaultIfEmpty()
                        group new { joined = j2, produto = p } by new { p.Id, p.Nome, p.CategoriaId, p.Preco, p.Status, p.Modificacao } into grouped
                        where grouped.Key.Id == id
                        select new ProdutoViewModel
                        {
                            CategoriaId = grouped.Key.CategoriaId,
                            EmEstoque = grouped.Key.Status,
                            Id = grouped.Key.Id,
                            Modificacao = grouped.Key.Modificacao,
                            Nome = grouped.Key.Nome,
                            Preco = grouped.Key.Preco,
                            QuantidadeVendida = grouped.Count(t => t.joined.ProdutoId == id),
                            VendaUnitaria = grouped.Sum(x => x.joined.Quantidade),
                        };

            return query.FirstOrDefault();
        }

        public IList<ProdutoViewModel> ListarViewModels(bool somenteEmEstoque, int idcategoria, bool semCategoria)
        {
            var query = from p in _context.Produtos
                        join c in _context.ProdutosPorCompras on p.Id equals c.ProdutoId into j1
                        from j2 in j1.DefaultIfEmpty()
                        group new { joined = j2, produto = p } by new { p.Id, p.Nome, p.CategoriaId, p.Preco, p.Status, p.Modificacao } into grouped
                        where (!somenteEmEstoque || grouped.Key.Status == true) &&
                        (idcategoria == 0 || grouped.Key.CategoriaId == idcategoria) &&
                        (!semCategoria || grouped.Key.CategoriaId == null)
                        select new ProdutoViewModel
                        {
                            EmEstoque = grouped.Key.Status,
                            Modificacao = grouped.Key.Modificacao,
                            Preco = grouped.Key.Preco,
                            CategoriaId = grouped.Key.CategoriaId,
                            Id = grouped.Key.Id,
                            Nome = grouped.Key.Nome,
                            QuantidadeVendida = grouped.Count(),
                            VendaUnitaria = grouped.Sum(x => x.joined.Quantidade),
                        };

            return query.ToList();
        }

        public IList<ProdutoViewModel> ListarViewModelsOrdenadoPorMaisVendido()
        {
            var query = from p in _context.Produtos
                        join c in _context.ProdutosPorCompras on p.Id equals c.ProdutoId into j1
                        from j2 in j1.DefaultIfEmpty()
                        group new { joined = j2, produto = p } by new { p.Id, p.Nome, p.CategoriaId, p.Preco, p.Status, p.Modificacao } into grouped
                        orderby grouped.Sum(x => x.joined.Quantidade) descending
                        select new ProdutoViewModel
                        {
                            EmEstoque = grouped.Key.Status,
                            Modificacao = grouped.Key.Modificacao,
                            Preco = grouped.Key.Preco,
                            CategoriaId = grouped.Key.CategoriaId,
                            Id = grouped.Key.Id,
                            Nome = grouped.Key.Nome,
                            QuantidadeVendida = grouped.Count(),
                            VendaUnitaria = grouped.Sum(x => x.joined.Quantidade),
                        };

            return query.ToList();
        }
    }
}
