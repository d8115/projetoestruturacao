﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Produto;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.ProdutoRepository
{
    public interface IProdutoRepository : IRepositorioBase<Produto>
    {
        IList<ProdutoViewModel> ListarViewModels(bool somenteEmEstoque, int idcategoria, bool semCategoria);
        IList<ProdutoViewModel> ListarViewModelsOrdenadoPorMaisVendido();
        ProdutoViewModel? SelecionarViewModel(int id);
    }
}
