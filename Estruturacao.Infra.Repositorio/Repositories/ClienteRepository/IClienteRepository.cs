﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Cliente;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.ClienteRepository
{
    public interface IClienteRepository : IRepositorioBase<Cliente>
    {
        bool ExistePorCPF(string cpf);
        IList<ClienteViewModel> ListarViewModels();
        IList<ClienteViewModel> ListarViewModelsOrdenadoPorGasto();
        IList<ClienteViewModel> ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto();
        IList<ClienteViewModel> ListarViewModelsordenadoPorQuantidadeProdutosComprados();
        ClienteViewModel? SelecionarViewModel(string? cpf, int id);
    }
}
