﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Cliente;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace Estruturacao.Infra.Repositorio.Repositories.ClienteRepository
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {
        public ClienteRepository(AppDbContext context) : base(context) { }
        public ClienteViewModel? SelecionarViewModel(string? cpf, int id)
        {
            var query = from clientes in _context.Clientes.AsNoTracking()
                        join compras in _context.Compras.AsNoTracking() on clientes.Id equals compras.ClienteId into leftjoinclientecompra
                        from resultclientecompraleftjoin in leftjoinclientecompra.DefaultIfEmpty()
                        join produtosporcompra in _context.ProdutosPorCompras.AsNoTracking() on resultclientecompraleftjoin.Id equals produtosporcompra.CompraId into leftjoinresultcomprdoutosporcompra
                        from resultprodutsporcompraleftcliente in leftjoinresultcomprdoutosporcompra.DefaultIfEmpty()
                        join produto in _context.Produtos.AsNoTracking() on resultprodutsporcompraleftcliente.ProdutoId equals produto.Id into leftjoinprodutocomresult
                        from resultprodutocomresult in leftjoinprodutocomresult.DefaultIfEmpty()
                        group new { clientes.Id, } by new
                        {
                            Cliente = new
                            {
                                clientes.Id,
                                clientes.CEP,
                                clientes.Cpf,
                                clientes.Data_Nascimento,
                                clientes.Email,
                                clientes.Modificacao,
                                clientes.Nome,
                                Compras = clientes.Compras.Count,
                            },
                        } into cc
                        where (string.IsNullOrEmpty(cpf) || cc.Key.Cliente.Cpf == cpf) && (id == 0 || cc.Key.Cliente.Id == id)
                        select new ClienteViewModel()
                        {

                            Cpf = cc.Key.Cliente.Cpf,
                            CEP = cc.Key.Cliente.CEP,
                            Data_Nascimento = cc.Key.Cliente.Data_Nascimento,
                            Email = cc.Key.Cliente.Email,
                            Modificacao = cc.Key.Cliente.Modificacao,
                            Nome = cc.Key.Cliente.Nome,
                            QuantidadeCompras = cc.Key.Cliente.Compras,
                        };

            return query.FirstOrDefault();
        }

        public bool ExistePorCPF(string cpf)
        {
            return _context.Clientes
                .Where(x => x.Cpf == cpf)
                .Any();
        }

        public IList<ClienteViewModel> ListarViewModels()
        {
            var query = from clientes in _context.Clientes.AsNoTracking()
                        join compras in _context.Compras.AsNoTracking() on clientes.Id equals compras.ClienteId into leftjoinclientecompra
                        from resultclientecompraleftjoin in leftjoinclientecompra.DefaultIfEmpty()
                        join produtosporcompra in _context.ProdutosPorCompras.AsNoTracking() on resultclientecompraleftjoin.Id equals produtosporcompra.CompraId into leftjoinresultcomprdoutosporcompra
                        from resultprodutsporcompraleftcliente in leftjoinresultcomprdoutosporcompra.DefaultIfEmpty()
                        join produto in _context.Produtos.AsNoTracking() on resultprodutsporcompraleftcliente.ProdutoId equals produto.Id into leftjoinprodutocomresult
                        from resultprodutocomresult in leftjoinprodutocomresult.DefaultIfEmpty()
                        group new { clientes.Id, } by new
                        {
                            Cliente = new
                            {
                                clientes.Id,
                                clientes.CEP,
                                clientes.Cpf,
                                clientes.Data_Nascimento,
                                clientes.Email,
                                clientes.Modificacao,
                                clientes.Nome,
                                Compras = clientes.Compras.Count,
                            },
                        } into cc
                        select new ClienteViewModel()
                        {

                            Cpf = cc.Key.Cliente.Cpf,
                            CEP = cc.Key.Cliente.CEP,
                            Data_Nascimento = cc.Key.Cliente.Data_Nascimento,
                            Email = cc.Key.Cliente.Email,
                            Modificacao = cc.Key.Cliente.Modificacao,
                            Nome = cc.Key.Cliente.Nome,
                            QuantidadeCompras = cc.Key.Cliente.Compras,
                        };

            return query.ToList();
        }

        public IList<ClienteViewModel> ListarViewModelsOrdenadoPorGasto()
        {
            var query = from clientes in _context.Clientes.AsNoTracking()
                        join compras in _context.Compras.AsNoTracking() on clientes.Id equals compras.ClienteId into clientescomprajoin
                        from resultclientescompra in clientescomprajoin.DefaultIfEmpty()
                        join produtosporcompra in _context.ProdutosPorCompras.AsNoTracking() on resultclientescompra.Id equals produtosporcompra.CompraId into produtosporcomprasjoin
                        from resultprodutosporcomprajoin in produtosporcomprasjoin.DefaultIfEmpty()
                        join produto in _context.Produtos.AsNoTracking() on resultprodutosporcomprajoin.ProdutoId equals produto.Id into produtojoin
                        from resultprodutojoin in produtojoin.DefaultIfEmpty()
                        group new
                        {
                            clientes.Id,
                            ResultPrecoMultQuantidade = resultprodutosporcomprajoin.Quantidade * resultprodutojoin.Preco,
                        } by new
                        {
                            Cliente = new
                            {
                                clientes.Id,
                                clientes.CEP,
                                clientes.Cpf,
                                clientes.Data_Nascimento,
                                clientes.Email,
                                clientes.Modificacao,
                                clientes.Nome,
                                Compras = clientes.Compras.Count,
                            },
                        } into cc
                        orderby cc.Sum(x => x.ResultPrecoMultQuantidade) descending
                        select new ClienteViewModel()
                        {

                            Cpf = cc.Key.Cliente.Cpf,
                            CEP = cc.Key.Cliente.CEP,
                            Data_Nascimento = cc.Key.Cliente.Data_Nascimento,
                            Email = cc.Key.Cliente.Email,
                            Modificacao = cc.Key.Cliente.Modificacao,
                            Nome = cc.Key.Cliente.Nome,
                            QuantidadeCompras = cc.Key.Cliente.Compras,
                        };

            return query.ToList();
        }

        public IList<ClienteViewModel> ListarViewModelsordenadoPorQuantidadeProdutosComprados()
        {
            var query = from clientes in _context.Clientes.AsNoTracking()
                        join compras in _context.Compras.AsNoTracking() on clientes.Id equals compras.ClienteId into clientescomprajoin
                        from resultclientescompra in clientescomprajoin.DefaultIfEmpty()
                        join produtosporcompra in _context.ProdutosPorCompras.AsNoTracking() on resultclientescompra.Id equals produtosporcompra.CompraId into produtosporcomprasjoin
                        from resultprodutosporcomprajoin in produtosporcomprasjoin.DefaultIfEmpty()
                        group new
                        {
                            clientes.Id,
                            QuantidadeProdutos = resultprodutosporcomprajoin.Quantidade
                        } by new
                        {
                            Cliente = new
                            {
                                clientes.Id,
                                clientes.CEP,
                                clientes.Cpf,
                                clientes.Data_Nascimento,
                                clientes.Email,
                                clientes.Modificacao,
                                clientes.Nome,
                                Compras = clientes.Compras.Count,
                            },
                        } into cc
                        orderby cc.Sum(x => x.QuantidadeProdutos) descending
                        select new ClienteViewModel()
                        {

                            Cpf = cc.Key.Cliente.Cpf,
                            CEP = cc.Key.Cliente.CEP,
                            Data_Nascimento = cc.Key.Cliente.Data_Nascimento,
                            Email = cc.Key.Cliente.Email,
                            Modificacao = cc.Key.Cliente.Modificacao,
                            Nome = cc.Key.Cliente.Nome,
                            QuantidadeCompras = cc.Key.Cliente.Compras,
                        };

            return query.ToList();
        }

        public IList<ClienteViewModel> ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto()
        {
            var query = from clientes in _context.Clientes.AsNoTracking()
                        join compras in _context.Compras.AsNoTracking() on clientes.Id equals compras.ClienteId into leftjoinclientecompra
                        from resultclientecompraleftjoin in leftjoinclientecompra.DefaultIfEmpty()
                        join produtosporcompra in _context.ProdutosPorCompras.AsNoTracking() on resultclientecompraleftjoin.Id equals produtosporcompra.CompraId into leftjoinresultcomprdoutosporcompra
                        from resultprodutsporcompraleftcliente in leftjoinresultcomprdoutosporcompra.DefaultIfEmpty()
                        join produto in _context.Produtos.AsNoTracking() on resultprodutsporcompraleftcliente.ProdutoId equals produto.Id into leftjoinprodutocomresult
                        from resultprodutocomresult in leftjoinprodutocomresult.DefaultIfEmpty()
                        orderby resultprodutocomresult.ProdutosProCompra.Sum(x => x.Quantidade) descending
                        group new
                        {
                            clientes.Id,
                            Produto = new
                            {
                                PrecoProduto = resultprodutocomresult.Preco,
                                Quantia = resultprodutsporcompraleftcliente.Quantidade,
                            }
                        } by new
                        {
                            Cliente = new
                            {
                                clientes.Id,
                                clientes.CEP,
                                clientes.Cpf,
                                clientes.Data_Nascimento,
                                clientes.Email,
                                clientes.Modificacao,
                                clientes.Nome,
                                Compras = clientes.Compras.Count,
                            },
                        } into cc
                        select new ClienteViewModel()
                        {

                            Cpf = cc.Key.Cliente.Cpf,
                            CEP = cc.Key.Cliente.CEP,
                            Data_Nascimento = cc.Key.Cliente.Data_Nascimento,
                            Email = cc.Key.Cliente.Email,
                            Modificacao = cc.Key.Cliente.Modificacao,
                            Nome = cc.Key.Cliente.Nome,
                            QuantidadeCompras = cc.Key.Cliente.Compras,
                        };

            return query.ToList();
        }
    }
}
