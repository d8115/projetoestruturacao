﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace Estruturacao.Infra.Repositorio.Repositories.CategoriaRepository
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(AppDbContext context) : base(context)
        {
        }

        public CategoriaViewModel? SelecionarViewModel(int id)
        {
            var categoria = from categorias in _context.Categorias.AsNoTracking()
                            join produtos in _context.Produtos.AsNoTracking() on categorias.Id equals produtos.CategoriaId
                            join produtoscompras in _context.ProdutosPorCompras.AsNoTracking() on produtos.Id equals produtoscompras.ProdutoId
                            where produtos.CategoriaId == categorias.Id
                            group new { categorias.Id, produtoscompras } by new { categorias.Id, categorias.Modificacao, categorias.Nome } into cc
                            where cc.Key.Id == id
                            select new CategoriaViewModel()
                            {
                                ProdutosVendidosComACategoria = cc.Sum(x => x.produtoscompras.Quantidade),
                                Nome = cc.Key.Nome,
                                Id = cc.Key.Id,
                                Modificacao = cc.Key.Modificacao,
                            };

            return categoria.FirstOrDefault();
        }

        public IList<CategoriaViewModel> ListarViewModels()
        {
            var categoria = from categorias in _context.Categorias.AsNoTracking()
                            join produtos in _context.Produtos.AsNoTracking() on categorias.Id equals produtos.CategoriaId
                            join produtoscompras in _context.ProdutosPorCompras.AsNoTracking() on produtos.Id equals produtoscompras.ProdutoId
                            where produtos.CategoriaId == categorias.Id
                            group new { categorias.Id, produtoscompras } by new { categorias.Id, categorias.Modificacao, categorias.Nome } into cc
                            select new CategoriaViewModel()
                            {
                                ProdutosVendidosComACategoria = cc.Sum(x => x.produtoscompras.Quantidade),
                                Nome = cc.Key.Nome,
                                Id = cc.Key.Id,
                                Modificacao = cc.Key.Modificacao,
                            };

            return categoria.ToList();
        }

        public IList<CategoriaQuantidadeVendidaViewModel> ListarProdutosCompradosPorCategoria()
        {
            var query = from produtosporcompras in _context.ProdutosPorCompras.AsNoTracking()
                        join produtos in _context.Produtos.AsNoTracking() on produtosporcompras.ProdutoId equals produtos.Id
                        join categorias in _context.Categorias.AsNoTracking() on produtos.CategoriaId equals categorias.Id into produtocategorialeftjoin
                        from resultleftjoin in produtocategorialeftjoin.DefaultIfEmpty()
                        group new { produtos.CategoriaId, produtosporcompras } by new { ProdutoId = produtos.Id, NomeDaCategoria = resultleftjoin.Nome ?? "Sem Categoria" } into pc
                        select new CategoriaQuantidadeVendidaViewModel
                        {
                            QuantidadeVendida = pc.Sum(x => x.produtosporcompras.Quantidade),
                            NomedaCategoria = pc.Key.NomeDaCategoria,
                        };

            return query.ToList();
        }
    }
}
