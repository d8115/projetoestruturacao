﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.CategoriaRepository
{
    public interface ICategoriaRepository : IRepositorioBase<Categoria>
    {
        IList<CategoriaQuantidadeVendidaViewModel> ListarProdutosCompradosPorCategoria();
        IList<CategoriaViewModel> ListarViewModels();
        CategoriaViewModel? SelecionarViewModel(int id);
    }
}