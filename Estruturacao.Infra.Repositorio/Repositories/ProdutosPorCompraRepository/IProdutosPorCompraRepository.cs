﻿using Estruturacao.Domain.Entities;
using Estruturacao.Domain.EntitiesInOut.ViewModel.ProdutosPorCompra;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.ProdutosPorCompraRepository
{
    public interface IProdutosPorCompraRepository : IRepositorioBase<ProdutosPorCompra>
    {
        IEnumerable<ProdutosPorCompraViewModel> Listar(int idCliente);
    }
}
