﻿using Estruturacao.Domain.Entities;
using Estruturacao.Domain.EntitiesInOut.ViewModel.ProdutosPorCompra;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.ProdutosPorCompraRepository
{
    public class ProdutosPorCompraRepository : BaseRepository<ProdutosPorCompra>, IProdutosPorCompraRepository
    {
        public ProdutosPorCompraRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<ProdutosPorCompraViewModel> Listar(int idCliente)
        {
            return _context.ProdutosPorCompras
                .AsEnumerable()
                .Select(ppc => new ProdutosPorCompraViewModel
                {
                    CompraId = ppc.CompraId,
                    ProdutoId = ppc.ProdutoId,
                    Quantidade = ppc.Quantidade,
                    Id = ppc.Id,
                    Modificacao = ppc.Modificacao,
                });
        }
    }
}
