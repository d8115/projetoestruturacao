﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Compra;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio.Repositories.Base;
using Estruturacao.Infra.Repositorio.Repositories.ClienteRepository;

namespace Estruturacao.Infra.Repositorio.Repositories.CompraRepository
{
    public class CompraRepository : BaseRepository<Compra>, ICompraRepository
    {
        public CompraRepository(AppDbContext context) : base(context) { }

        public IList<CompraViewModel> ListarViewModel(int idProduto, int idCliente, string? cpfCliente)
        {
            var comprasQuery = from compras in _context.Compras
                               join clientes in _context.Clientes on compras.ClienteId equals clientes.Id
                               join produtos in _context.ProdutosPorCompras on compras.Id equals produtos.CompraId
                               where (idProduto == 0 || produtos.ProdutoId == idProduto)
                               && (idCliente == 0 || clientes.Id == idCliente)
                               && (string.IsNullOrEmpty(cpfCliente) || clientes.Cpf == cpfCliente)
                               group new { compras, produtos } by new { compras.ClienteId, produtos.CompraId } into a
                               select new CompraViewModel
                               {
                                   DataCompra = a.Select(c => c.compras.Modificacao).FirstOrDefault(),
                                   IdCompra = a.Select(c => c.compras.Id).FirstOrDefault(),
                                   IdComprador = a.Select(c => c.compras.ClienteId).FirstOrDefault(),
                                   QuantidadeProdutos = a.Select(c => c.produtos.Quantidade).Sum()
                               };

            return comprasQuery.ToList();
        }
    }
}
