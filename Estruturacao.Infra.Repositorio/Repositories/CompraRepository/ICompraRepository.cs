﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Compra;
using Estruturacao.Infra.Repositorio.Repositories.Base;

namespace Estruturacao.Infra.Repositorio.Repositories.CompraRepository
{
    public interface ICompraRepository : IRepositorioBase<Compra>
    {
        public IList<CompraViewModel> ListarViewModel(int idProduto, int idCliente, string? cpfCliente);
    }
}
