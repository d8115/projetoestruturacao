﻿using Estruturacao.Infra.DataBaseEntity.Contexto;
using Microsoft.EntityFrameworkCore;

namespace Estruturacao.Infra.Repositorio.Repositories.Base
{
    public abstract class BaseRepository<T> : IRepositorioBase<T> where T : class
    {
        protected readonly AppDbContext _context;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
        }
        public T Adicionar(T request)
        {
            _context.Set<T>().Add(request);
            _context.SaveChanges();

            return request;
        }

        public void Deletar(T request)
        {
            _context.Set<T>().Remove(request);
            _context.SaveChanges();
        }

        public void Editar(T request)
        {
            _context.Entry(request).State = EntityState.Modified;
            _context.Set<T>().Update(request);
            _context.SaveChanges();
        }

        public IEnumerable<T> Listar()
        {
            return _context.Set<T>().AsEnumerable<T>();
        }

        public T? SelecionarPorId(int? id)
        {
            return _context.Set<T>().Find(id);
        }
    }
}
