﻿namespace Estruturacao.Infra.Repositorio.Repositories.Base
{
    public interface IRepositorioBase<T> where T : class
    {
        void Editar(T request);
        T Adicionar(T request);
        IEnumerable<T> Listar();
        T? SelecionarPorId(int? id);
        void Deletar(T request);
    }

}
