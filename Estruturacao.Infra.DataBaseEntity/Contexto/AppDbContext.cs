﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.Entities;
using Estruturacao.Infra.DataBaseEntity.Mapeamentos;
using Microsoft.EntityFrameworkCore;

namespace Estruturacao.Infra.DataBaseEntity.Contexto
{
    public class AppDbContext : DbContext
    {
        public DbSet<Compra> Compras { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<ProdutosPorCompra> ProdutosPorCompras { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MapeamentoCliente).Assembly);
        }
    }
}
