﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Estruturacao.Infra.DataBaseEntity.Migrations
{
    public partial class ReajustandoTabelaCompra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClienteCpf",
                table: "Compras");

            migrationBuilder.AddColumn<int>(
                name: "ClienteId",
                table: "Compras",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClienteId",
                table: "Compras");

            migrationBuilder.AddColumn<string>(
                name: "ClienteCpf",
                table: "Compras",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
