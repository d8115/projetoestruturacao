﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Estruturacao.Infra.DataBaseEntity.Migrations
{
    public partial class InttoDecimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_produtosPorCompras_Compras_CompraId",
                table: "produtosPorCompras");

            migrationBuilder.DropForeignKey(
                name: "FK_produtosPorCompras_Produtos_ProdutoId",
                table: "produtosPorCompras");

            migrationBuilder.DropPrimaryKey(
                name: "PK_produtosPorCompras",
                table: "produtosPorCompras");

            migrationBuilder.DropColumn(
                name: "Quantidade",
                table: "Compras");

            migrationBuilder.RenameTable(
                name: "produtosPorCompras",
                newName: "ProdutosPorCompras");

            migrationBuilder.RenameIndex(
                name: "IX_produtosPorCompras_ProdutoId",
                table: "ProdutosPorCompras",
                newName: "IX_ProdutosPorCompras_ProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_produtosPorCompras_CompraId",
                table: "ProdutosPorCompras",
                newName: "IX_ProdutosPorCompras_CompraId");

            migrationBuilder.AlterColumn<decimal>(
                name: "Quantidade",
                table: "ProdutosPorCompras",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProdutosPorCompras",
                table: "ProdutosPorCompras",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutosPorCompras_Compras_CompraId",
                table: "ProdutosPorCompras",
                column: "CompraId",
                principalTable: "Compras",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutosPorCompras_Produtos_ProdutoId",
                table: "ProdutosPorCompras",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutosPorCompras_Compras_CompraId",
                table: "ProdutosPorCompras");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutosPorCompras_Produtos_ProdutoId",
                table: "ProdutosPorCompras");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProdutosPorCompras",
                table: "ProdutosPorCompras");

            migrationBuilder.RenameTable(
                name: "ProdutosPorCompras",
                newName: "produtosPorCompras");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutosPorCompras_ProdutoId",
                table: "produtosPorCompras",
                newName: "IX_produtosPorCompras_ProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutosPorCompras_CompraId",
                table: "produtosPorCompras",
                newName: "IX_produtosPorCompras_CompraId");

            migrationBuilder.AlterColumn<int>(
                name: "Quantidade",
                table: "produtosPorCompras",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.AddColumn<int>(
                name: "Quantidade",
                table: "Compras",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_produtosPorCompras",
                table: "produtosPorCompras",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_produtosPorCompras_Compras_CompraId",
                table: "produtosPorCompras",
                column: "CompraId",
                principalTable: "Compras",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_produtosPorCompras_Produtos_ProdutoId",
                table: "produtosPorCompras",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
