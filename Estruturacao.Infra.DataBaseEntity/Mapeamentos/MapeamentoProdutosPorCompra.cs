﻿using Estruturacao.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estruturacao.Infra.DataBaseEntity.Mapeamentos
{
    public class MapeamentoProdutosPorCompra : IEntityTypeConfiguration<ProdutosPorCompra>
    {
        public void Configure(EntityTypeBuilder<ProdutosPorCompra> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasOne(p => p.Produto)
                .WithMany(p => p.ProdutosProCompra)
                .HasForeignKey(p => p.ProdutoId);

            builder.HasOne(c => c.Compra)
                .WithMany(c => c.ProdutosCompra)
                .HasForeignKey(c => c.CompraId);

            builder.Property(p => p.Quantidade)
                .IsRequired();
        }
    }
}
