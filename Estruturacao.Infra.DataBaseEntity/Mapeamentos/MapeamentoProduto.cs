﻿using Estruturacao.Domain.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estruturacao.Infra.DataBaseEntity.Mapeamentos
{
    public class MapeamentoProduto : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Categoria)
                .WithMany(p => p.Produto)
                .HasForeignKey(p => p.CategoriaId);

            builder.Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(p => p.Preco)
                .IsRequired();
        }
    }
}
