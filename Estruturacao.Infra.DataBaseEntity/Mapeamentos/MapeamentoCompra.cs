﻿using Estruturacao.Domain.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estruturacao.Infra.DataBaseEntity.Mapeamentos
{
    public class MapeamentoCompra : IEntityTypeConfiguration<Compra>
    {
        public void Configure(EntityTypeBuilder<Compra> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasOne(c => c.Cliente)
                .WithMany(c => c.Compras)
                .HasForeignKey(c => c.ClienteId);
        }
    }
}
