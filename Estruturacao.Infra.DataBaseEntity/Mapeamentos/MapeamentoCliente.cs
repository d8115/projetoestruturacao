﻿using Estruturacao.Domain.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estruturacao.Infra.DataBaseEntity.Mapeamentos
{
    public class MapeamentoCliente : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasIndex(c => c.Cpf)
                .IsUnique();

            builder.Property(c => c.Cpf)
                .IsRequired()
                .HasMaxLength(11);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(c => c.Data_Nascimento)
                .IsRequired();

            builder.Property(c => c.CEP)
                .IsRequired()
                .HasMaxLength(8);
        }
    }
}
