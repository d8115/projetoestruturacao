using Estruturacao.API;

var builder = WebApplication.CreateBuilder(args);

Startup startup = new(builder.Configuration);

startup.Run(builder, builder.Services);