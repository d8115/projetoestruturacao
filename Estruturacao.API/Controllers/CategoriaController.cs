﻿using Estruturacao.Domain.EntitiesInOut.DTO.Categoria;
using Estruturacao.Domain.Interfaces.Adapters;
using Microsoft.AspNetCore.Mvc;

namespace Estruturacao.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriaController : Controller
    {
        private readonly IAplicacaoCategoria _service;
        public CategoriaController(IAplicacaoCategoria service)
        {
            _service = service;
        }

        [HttpPost("criar")]
        public IActionResult CriarCategoria([FromBody] CategoriaDTO categoria)
        {
            try
            {
                _service.Adicionar(categoria);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("deletar")]
        public IActionResult DeletarCategoria([FromQuery] int id)
        {
            try
            {
                _service.Deletar(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("editar/{id}")]
        public IActionResult EditarCategoria([FromBody] CategoriaDTO categoria, int id)
        {
            try
            {
                _service.Alterar(id, categoria);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscar")]
        public IActionResult BuscaCategoria([FromQuery] int id)
        {
            try
            {
                var result = _service.Selecionar(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("todos")]
        public IActionResult BuscarTodos()
        {
            try
            {
                var result = _service.Listar();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("todosporquantidadevendida")]
        public IActionResult BuscarPorQuantidadeVendida()
        {
            try
            {
                var result = _service.ListarPorQuantidadeVendida();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
