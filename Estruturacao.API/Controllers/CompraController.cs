﻿using Estruturacao.Domain.EntitiesInOut.DTO.Compra;
using Estruturacao.Domain.Interfaces.Adapters;
using Microsoft.AspNetCore.Mvc;

namespace Estruturacao.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompraController : Controller
    {
        private readonly IAplicacaoCompra _adapter;
        public CompraController(IAplicacaoCompra adapter)
        {
            _adapter = adapter;
        }

        [HttpPost("comprar")]
        public IActionResult ComprarProduto([FromBody] CompraDTO compraDTO)
        {
            try
            {
                _adapter.Comprar(compraDTO);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("listarCompras")]
        public IActionResult ListarTodasAsCompras(int idProduto, int idCliente, string? cpfCliente)
        {
            try
            {
                var result = _adapter.Listar(idProduto, idCliente, cpfCliente);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
