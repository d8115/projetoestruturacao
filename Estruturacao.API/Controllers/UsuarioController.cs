﻿using Estruturacao.Domain.Adapters;
using Estruturacao.Domain.EntitiesInOut.In;
using Microsoft.AspNetCore.Mvc;

namespace Estruturacao.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly IAplicacaoCliente _clienteAdapter;
        public UsuarioController(IAplicacaoCliente clienteAdapater)
        {
            _clienteAdapter = clienteAdapater;
        }

        [HttpPost("criar")]
        public IActionResult CriaUsuario([FromBody] ClienteDTO cliente)
        {
            try
            {
                _clienteAdapter.Adicionar(cliente);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("deletar")]
        public IActionResult DeletaUsuario([FromQuery] int id)
        {
            try
            {
                _clienteAdapter.Deletar(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("editar")]
        public IActionResult EditaUsuario([FromQuery] int id, [FromBody] ClienteDTO cliente)
        {
            try
            {
                _clienteAdapter.Alterar(cliente, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscar")]
        public IActionResult BuscaUsuario(string? cpf, int id)
        {
            try
            {
                var result = _clienteAdapter.Selecionar(cpf, id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("todos")]
        public IActionResult BuscarTodos()
        {
            try
            {
                var result = _clienteAdapter.Listar();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("todosOrdenadosPorGasto")]
        public IActionResult BuscarTodosOrdenadoPorGasto()
        {
            try
            {
                var result = _clienteAdapter.ListarClienteOrdenadoPorGasto();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("todosOrdenadosPorQuantidadeDeCompra")]
        public IActionResult BuscarTodosOrdenadoPorQuantidadeDeCompra()
        {
            try
            {
                var result = _clienteAdapter.ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("todosOrdenadosPorQuantidadeProdutosComprados")]
        public IActionResult BuscarTodosOrdenadosPorQuantidadeProdutosComprados()
        {
            try
            {
                var result = _clienteAdapter.ListarViewModelsordenadoPorQuantidadeProdutosComprados();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscarEnderecoPorCEPCliente")]
        public IActionResult BuscarEnderecoPorCEP(int idCliente)
        {
            try
            {
                var result = _clienteAdapter.ExibirInformacoesCEPPorIdCliente(idCliente);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
