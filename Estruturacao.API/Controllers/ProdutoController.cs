﻿using Estruturacao.Domain.Adapters;
using Estruturacao.Domain.EntitiesInOut.DTO.Produto;
using Microsoft.AspNetCore.Mvc;

namespace Estruturacao.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        private readonly IAplicacaoProduto _service;
        public ProdutoController(IAplicacaoProduto service)
        {
            _service = service;
        }

        [HttpPost("criar")]
        public IActionResult CriarProduto([FromBody] ProdutoDTO produto)
        {
            try
            {
                _service.Adicionar(produto);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("deletar")]
        public IActionResult DeletarProduto([FromQuery] int id)
        {
            try
            {
                _service.Deletar(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("editar/{id}")]
        public IActionResult EditarProduto([FromBody] ProdutoDTO produto, int id)
        {
            try
            {
                _service.Alterar(produto, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscar")]
        public IActionResult BuscarProduto([FromQuery] int id)
        {
            try
            {
                var result = _service.Selecionar(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscarProdutos")]
        public IActionResult BuscarProdutos(bool somenteEstoque, int idCategoria, bool semCategoria)
        {
            try
            {
                var result = _service.Listar(somenteEstoque, idCategoria, semCategoria);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("buscarProdutosOrdenadosPorMaisVendidos")]
        public IActionResult ListarProdutosPorMaisVendidos()
        {
            try
            {
                var result = _service.ListarPorMaisVendido();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("desativar")]
        public IActionResult DesativarProduto([FromQuery] int id)
        {
            try
            {
                _service.Desativar(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
