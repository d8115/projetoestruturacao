﻿using Estruturacao.Application;
using Estruturacao.Infra.DataBaseEntity.Contexto;
using Estruturacao.Infra.Repositorio;
using Microsoft.EntityFrameworkCore;

namespace Estruturacao.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void Run(WebApplicationBuilder ApplicationBuilder, IServiceCollection builder)
        {
            ConfigureServices(builder);
            var app = ApplicationBuilder.Build();
            Configure(app, app.Environment);
            app.Run();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRepositorioDependency();
            services.AddApplicationModule();
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddDbContext<AppDbContext>(options => options.UseMySql(Configuration.GetConnectionString("MySql"), new MySqlServerVersion(new Version(8, 0))));
        }

        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
