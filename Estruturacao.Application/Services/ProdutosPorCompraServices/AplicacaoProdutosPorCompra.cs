﻿using Estruturacao.Application.Services.BaseServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Entities;
using Estruturacao.Domain.Interfaces.Adapters;
using Estruturacao.Infra.Repositorio.Repositories.ProdutosPorCompraRepository;

namespace Estruturacao.Application.Services.ProdutosPorCompraServices
{
    public class AplicacaoProdutosPorCompra : BaseServiceManager, IAplicacaoProdutosPorCompras
    {
        private readonly IProdutosPorCompraRepository _produtosPorCompraRepository;

        public AplicacaoProdutosPorCompra(IUnidadeTrabalho unidadeTrabalho, IProdutosPorCompraRepository produtosPorCompraRepository) : base(unidadeTrabalho)
        {
            _produtosPorCompraRepository = produtosPorCompraRepository;
        }

        public IList<ProdutosPorCompra> Listar()
        {
            return _produtosPorCompraRepository.Listar().ToList();
        }
    }
}
