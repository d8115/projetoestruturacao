﻿using Estruturacao.Application.Services.BaseServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Adapters;
using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Produto;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Produto;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.ProdutoRepository;

namespace Estruturacao.Application.Services.ProdutoServices
{
    public class AplicacaoProduto : BaseServiceManager, IAplicacaoProduto
    {
        private readonly IProdutoRepository _repository;
        private readonly IRegraNegocioProduto _regraNegocioProduto;

        public AplicacaoProduto(IUnidadeTrabalho unidadeTrabalho, IProdutoRepository repository, IRegraNegocioProduto regraNegocioProduto) : base(unidadeTrabalho)
        {
            _repository = repository;
            _regraNegocioProduto = regraNegocioProduto;
        }

        public void Adicionar(ProdutoDTO request)
        {
            Iniciar();
            _regraNegocioProduto.Incluir(request);
            Confirmar();
        }

        public void Alterar(ProdutoDTO request, int id)
        {
            Iniciar();

            Produto produtoBuscado = _regraNegocioProduto.Validar(id, true, request);

            produtoBuscado.Nome = request.Nome;
            produtoBuscado.Status = produtoBuscado.Status;
            produtoBuscado.CategoriaId = request.CategoriaId;
            produtoBuscado.Modificacao = DateTime.Now;
            produtoBuscado.Preco = request.Preco;

            _repository.Editar(produtoBuscado);
            Confirmar();
        }

        public void Deletar(int id)
        {
            Iniciar();
            Produto produto = _regraNegocioProduto.Validar(id, true, null);
            _repository.Deletar(produto);
            Confirmar();
        }

        public void Desativar(int id)
        {
            Iniciar();
            Produto produto = _regraNegocioProduto.Validar(id, false, null);
            produto.Status = false;
            _repository.Editar(produto);
            Confirmar();
        }

        public IList<ProdutoViewModel> Listar(bool somenteEstoque, int idCategoria, bool semCategoria)
        {
            return _repository.ListarViewModels(somenteEstoque, idCategoria, semCategoria);
        }

        public IList<ProdutoViewModel> ListarPorMaisVendido()
        {
            return _repository.ListarViewModelsOrdenadoPorMaisVendido();
        }

        public ProdutoViewModel? Selecionar(int id)
        {
            return _repository.SelecionarViewModel(id);
        }
    }
}
