﻿using Estruturacao.Application.Services.BaseServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Categoria;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Categoria;
using Estruturacao.Domain.Interfaces.Adapters;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.CategoriaRepository;

namespace Estruturacao.Application.Services.CategoriaServices
{
    public class AplicacaoCategoria : BaseServiceManager, IAplicacaoCategoria
    {
        private readonly ICategoriaRepository _repository;
        private readonly IRegraNegocioCategoria _regraNegocioCategoria;

        public AplicacaoCategoria(IUnidadeTrabalho unidadeTrabalho, ICategoriaRepository repository, IRegraNegocioCategoria regraNegocioCategoria) : base(unidadeTrabalho)
        {
            _regraNegocioCategoria = regraNegocioCategoria;
            _repository = repository;
        }

        public void Adicionar(CategoriaDTO request)
        {
            Iniciar();
            _regraNegocioCategoria.Incluir(request);
            Confirmar();
        }

        public void Alterar(int id, CategoriaDTO request)
        {
            Iniciar();
            Categoria categoria = _regraNegocioCategoria.Validar(id);

            categoria.Nome = request.Nome;

            _repository.Editar(categoria);
            Confirmar();
        }

        public IEnumerable<CategoriaViewModel> Listar()
        {
            return _repository.ListarViewModels();
        }

        public IEnumerable<CategoriaQuantidadeVendidaViewModel> ListarPorQuantidadeVendida()
        {
            return _repository.ListarProdutosCompradosPorCategoria();
        }

        public CategoriaViewModel? Selecionar(int id)
        {
            return _repository.SelecionarViewModel(id);
        }

        public void Deletar(int id)
        {
            Iniciar();
            Categoria categoria = _regraNegocioCategoria.Validar(id, true);
            _repository.Deletar(categoria);
            Confirmar();
        }
    }
}
