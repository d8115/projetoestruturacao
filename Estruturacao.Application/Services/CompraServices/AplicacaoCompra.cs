﻿using Estruturacao.Application.Services.BaseServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Entities;
using Estruturacao.Domain.EntitiesInOut.DTO.Compra;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Compra;
using Estruturacao.Domain.Interfaces.Adapters;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.CompraRepository;

namespace Estruturacao.Application.Services.CompraServices
{
    public class AplicacaoCompra : BaseServiceManager, IAplicacaoCompra
    {
        private readonly ICompraRepository _repository;
        private readonly IRegraNegocioCompra _regraNegocioCompra;
        private readonly IRegraNegocioProdutosPorCompra _regraNegocioProdutosPorCompra;
        private readonly IRegraNegocioProduto _regraNegocioProduto;

        public AplicacaoCompra(IUnidadeTrabalho unidadeTrabalho,
            ICompraRepository repository,
            IRegraNegocioCompra regraNegocioCompra,
            IRegraNegocioProdutosPorCompra regraNegocioProdutosPorCompra,
            IRegraNegocioProduto regraNegocioProduto) : base(unidadeTrabalho)
        {
            _regraNegocioCompra = regraNegocioCompra;
            _repository = repository;
            _regraNegocioProdutosPorCompra = regraNegocioProdutosPorCompra;
            _regraNegocioProduto = regraNegocioProduto;
        }

        public void Comprar(CompraDTO compraDTO)
        {
            Iniciar();

            int c = _regraNegocioCompra.Incluir(compraDTO);

            for (var i = 0; i < compraDTO.ProdutosComprados.Count; i++)
            {

                ProdutosPorCompra p = new()
                {
                    ProdutoId = compraDTO.ProdutosComprados[i].IdProduto,
                    Quantidade = compraDTO.ProdutosComprados[i].Quantidade,
                    Modificacao = DateTime.Now,
                    CompraId = c
                };

                _regraNegocioProduto.Validar(compraDTO.ProdutosComprados[i].IdProduto, false, null);

                _regraNegocioProdutosPorCompra.Incluir(p);
            }

            Confirmar();
        }

        public IEnumerable<CompraViewModel> Listar(int idProduto, int idCliente, string? cpfCliente)
        {
            return _repository.ListarViewModel(idProduto, idCliente, cpfCliente);
        }
    }
}
