﻿using Estruturacao.Application.Services.BaseServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.CrossCutting.CEPServices;
using Estruturacao.CrossCutting.CEPServices.Entities;
using Estruturacao.CrossCutting.CSVServices;
using Estruturacao.Domain.Adapters;
using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.In;
using Estruturacao.Domain.EntitiesInOut.ViewModel.Cliente;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.ClienteRepository;

namespace Estruturacao.Application.Services.ClienteServices
{
    public class AplicacaoCliente : BaseServiceManager, IAplicacaoCliente
    {
        private readonly IClienteRepository _repository;
        private readonly IRegraNegocioCliente _regraNegocio;
        public AplicacaoCliente(IUnidadeTrabalho unidadeTrabalho, IClienteRepository repository, IRegraNegocioCliente regraNegocioCliente) : base(unidadeTrabalho)
        {
            _regraNegocio = regraNegocioCliente;
            _repository = repository;
        }
        public void Adicionar(ClienteDTO request)
        {
            Iniciar();
            _regraNegocio.IncluirLista(new List<ClienteDTO>() { request });
            Confirmar();
        }

        public void AdicionarLista(List<ClienteDTO> list)
        {
            Iniciar();
            _regraNegocio.IncluirLista(list);
            Confirmar();
        }

        public void Alterar(ClienteDTO request, int id)
        {
            Iniciar();
            Cliente cliente = _regraNegocio.Validar(id);

            cliente.Id = id;
            cliente.CEP = request.CEP;
            cliente.Cpf = request.Cpf;
            cliente.Data_Nascimento = request.Data_Nascimento;
            cliente.Email = request.Email;
            cliente.Modificacao = DateTime.Now;
            cliente.Nome = request.Nome;

            _repository.Editar(cliente);
            Confirmar();
        }

        public void Deletar(int id)
        {
            Iniciar();
            Cliente cliente = _regraNegocio.Validar(id, true);
            _repository.Deletar(cliente);
            Confirmar();
        }

        public IEnumerable<ClienteViewModel> Listar()
        {
            return _repository.ListarViewModels();
        }

        public async void SalvarInformacoesCSVNoBancoDeDados()
        {
            var resultadoLeitura = await LerCSV.Ler<ClienteDTO>(@"./test.csv");

            Iniciar();
            _regraNegocio.IncluirLista(resultadoLeitura.ToList());
            Confirmar();
        }

        public IEnumerable<ClienteViewModel> ListarClienteOrdenadoPorGasto()
        {
            return _repository.ListarViewModelsOrdenadoPorGasto();
        }

        public IEnumerable<ClienteViewModel> ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto()
        {
            return _repository.ListarViewModelsOrdenadoPorQuantidadeDeCompraDeProduto();
        }

        public IEnumerable<ClienteViewModel> ListarViewModelsordenadoPorQuantidadeProdutosComprados()
        {
            return _repository.ListarViewModelsordenadoPorQuantidadeProdutosComprados();
        }

        public ClienteViewModel? Selecionar(string? cpf, int id)
        {
            return _repository.SelecionarViewModel(cpf, id);
        }

        public InformacoesCEP? ExibirInformacoesCEPPorIdCliente(int idCliente)
        {

            var Cliente = _regraNegocio.Validar(idCliente, false);

            var cepInfos = BuscarInformacoesCEP.Buscar(Cliente.CEP);

            return cepInfos;
        }
    }
}
