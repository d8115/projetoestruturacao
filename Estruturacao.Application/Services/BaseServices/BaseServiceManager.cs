﻿using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Interfaces.Services;

namespace Estruturacao.Application.Services.BaseServices
{
    public abstract class BaseServiceManager : IBaseServiceManager
    {
        private readonly IUnidadeTrabalho _unidadeTrabalho;
        protected BaseServiceManager(IUnidadeTrabalho unidadeTrabalho)
        {
            _unidadeTrabalho = unidadeTrabalho;
        }
        public void Iniciar() => _unidadeTrabalho.Iniciar();

        public void Confirmar() => _unidadeTrabalho.Confirmar();

        public void Desfazer() => _unidadeTrabalho.Desfazer();

        public void Dispose() => _unidadeTrabalho.Dispose();
    }
}
