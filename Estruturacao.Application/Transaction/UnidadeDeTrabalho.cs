﻿using Estruturacao.Infra.DataBaseEntity.Contexto;

namespace Estruturacao.Application.Services.Transaction
{
    public class UnidadeTrabalho : IUnidadeTrabalho, IDisposable
    {
        private readonly AppDbContext _context;
        public UnidadeTrabalho(AppDbContext context)
        {
            _context = context;
        }

        public void Iniciar()
        {
            _context.Database.BeginTransaction();
        }

        public void Confirmar()
        {
            _context.Database.CommitTransaction();
        }

        public void Desfazer()
        {
            _context.Database.RollbackTransaction();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}