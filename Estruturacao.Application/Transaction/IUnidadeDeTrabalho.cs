﻿namespace Estruturacao.Application.Services.Transaction
{
    public interface IUnidadeTrabalho
    {
        void Iniciar();
        void Confirmar();
        void Desfazer();
        void Dispose();
    }
}
