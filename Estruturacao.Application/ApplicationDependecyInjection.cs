﻿using Estruturacao.Application.RegraNegocio.RegraNegocioCategoria;
using Estruturacao.Application.RegraNegocio.RegraNegocioCliente;
using Estruturacao.Application.RegraNegocio.RegraNegocioCompra;
using Estruturacao.Application.RegraNegocio.RegraNegocioProduto;
using Estruturacao.Application.RegraNegocio.RegraNegocioProdutosPorCompra;
using Estruturacao.Application.Services.CategoriaServices;
using Estruturacao.Application.Services.ClienteServices;
using Estruturacao.Application.Services.CompraServices;
using Estruturacao.Application.Services.ProdutoServices;
using Estruturacao.Application.Services.ProdutosPorCompraServices;
using Estruturacao.Application.Services.Transaction;
using Estruturacao.Domain.Adapters;
using Estruturacao.Domain.Interfaces.Adapters;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Microsoft.Extensions.DependencyInjection;

namespace Estruturacao.Application
{
    public static class ApplicationDependecyInjection
    {
        public static void AddApplicationModule(this IServiceCollection services)
        {
            //Adapters------------------------------------------------------------------------------------
            services.AddTransient<IAplicacaoCliente, AplicacaoCliente>();
            services.AddTransient<IAplicacaoCategoria, AplicacaoCategoria>();
            services.AddTransient<IAplicacaoProduto, AplicacaoProduto>();
            services.AddTransient<IUnidadeTrabalho, UnidadeTrabalho>();
            services.AddTransient<IAplicacaoCompra, AplicacaoCompra>();
            services.AddTransient<IAplicacaoProdutosPorCompras, AplicacaoProdutosPorCompra>();

            //Regras Negocios------------------------------------------------------------------------------
            services.AddTransient<IRegraNegocioCategoria, RegraNegocioCategoria>();
            services.AddTransient<IRegraNegocioProduto, RegraNegocioProduto>();
            services.AddTransient<IRegraNegocioCliente, RegraNegocioCliente>();
            services.AddTransient<IRegraNegocioCompra, RegraNegocioCompra>();
            services.AddTransient<IRegraNegocioProdutosPorCompra, RegraNegocioProdutosPorCompra>();
        }
    }
}
