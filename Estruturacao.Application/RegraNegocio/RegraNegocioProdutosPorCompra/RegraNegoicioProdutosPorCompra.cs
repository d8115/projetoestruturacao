﻿using Estruturacao.Domain.Entities;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.ProdutosPorCompraRepository;

namespace Estruturacao.Application.RegraNegocio.RegraNegocioProdutosPorCompra
{
    public class RegraNegocioProdutosPorCompra : IRegraNegocioProdutosPorCompra
    {
        private readonly IProdutosPorCompraRepository _produtosPorCompraRepository;

        public RegraNegocioProdutosPorCompra(IProdutosPorCompraRepository produtosPorCompraRepository)
        {
            _produtosPorCompraRepository = produtosPorCompraRepository;
        }

        public void Incluir(ProdutosPorCompra produtosPorCompra)
        {
            _produtosPorCompraRepository.Adicionar(produtosPorCompra);
        }
    }
}
