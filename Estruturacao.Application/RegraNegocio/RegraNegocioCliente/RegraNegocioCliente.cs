﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.In;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.ClienteRepository;

namespace Estruturacao.Application.RegraNegocio.RegraNegocioCliente
{
    public class RegraNegocioCliente : IRegraNegocioCliente
    {
        private readonly IClienteRepository _clienteRepository;
        public RegraNegocioCliente(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public void IncluirLista(IList<ClienteDTO> lista)
        {
            foreach(ClienteDTO clienteDTO in lista)
            {
                if (_clienteRepository.ExistePorCPF(clienteDTO.Cpf))
                {
                    throw new ArgumentException($"CPF {clienteDTO.Cpf} já cadastrado");
                }

                if (clienteDTO.CEP.Length > 8)
                {
                    throw new ArgumentException($"CEP {clienteDTO.CEP} excede o tamanho maximo");
                }

                Cliente cliente = new()
                {
                    CEP = clienteDTO.CEP,
                    Cpf = clienteDTO.Cpf,
                    Data_Nascimento = clienteDTO.Data_Nascimento,
                    Email = clienteDTO.Email,
                    Modificacao = DateTime.Now,
                    Nome = clienteDTO.Nome,
                };

                _clienteRepository.Adicionar(cliente);
            }
        }

        public Cliente Validar(int id, bool delete = false)
        {
            Cliente? cliente = _clienteRepository.SelecionarPorId(id);

            if (cliente == null)
            {
                throw new ArgumentException("Cliente Inexistente");
            }

            return cliente;
        }
    }
}
