﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Compra;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.CompraRepository;

namespace Estruturacao.Application.RegraNegocio.RegraNegocioCompra
{
    public class RegraNegocioCompra : IRegraNegocioCompra
    {
        private readonly ICompraRepository _compraRepository;
        private readonly IRegraNegocioCliente _regraNegocioCliente;

        public RegraNegocioCompra(ICompraRepository compraRepository, IRegraNegocioCliente regraNegocioCliente)
        {
            _compraRepository = compraRepository;
            _regraNegocioCliente = regraNegocioCliente;
        }

        public int Incluir(CompraDTO compraDTO)
        {
            Cliente cliente = _regraNegocioCliente.Validar(compraDTO.IdComprador, false);

            Compra compra = new()
            {
                ClienteId = cliente.Id,
                Modificacao = DateTime.Now,
            };

            Compra? adicaoResponse = _compraRepository.Adicionar(compra);

            return adicaoResponse.Id;
        }
    }
}
