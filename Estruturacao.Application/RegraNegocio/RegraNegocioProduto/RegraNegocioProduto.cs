﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Produto;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.ProdutoRepository;

namespace Estruturacao.Application.RegraNegocio.RegraNegocioProduto
{
    public class RegraNegocioProduto : IRegraNegocioProduto
    {
        private readonly IProdutoRepository _produtoRepository;
        public RegraNegocioProduto(IProdutoRepository produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public Produto Validar(int id, bool alteracao, ProdutoDTO? produtoDTO)
        {
            Produto? produto = _produtoRepository.SelecionarPorId(id);

            if (produto is null)
            {
                throw new NullReferenceException("Não existe produto com esse ID");
            }

            if (produto.Status == false && !alteracao)
            {
                throw new TypeAccessException("Produto Fora de Estoque");
            }

            if (produtoDTO != null)
            {
                ValidacaoPadrao(produtoDTO);
            }

            return produto;
        }

        public void Incluir(ProdutoDTO produtoDTO)
        {
            ValidacaoPadrao(produtoDTO);

            Produto produto = new()
            {
                CategoriaId = produtoDTO.CategoriaId,
                Modificacao = DateTime.Now,
                Nome = produtoDTO.Nome,
                Preco = produtoDTO.Preco,
                Status = true,
            };

            _produtoRepository.Adicionar(produto);
        }

        private static void ValidacaoPadrao(ProdutoDTO produtoDTO)
        {
            if (produtoDTO.CategoriaId == 0)
            {
                produtoDTO.CategoriaId = null;
            }

            if (produtoDTO.Nome.Length > 30)
            {
                throw new ArgumentException("Nome do Produto Muito Grande");
            }

            if (produtoDTO.Nome.Length < 2)
            {
                throw new ArgumentException("Nome do Produto Muito Curto");
            }

            if (produtoDTO.Preco < 1)
            {
                throw new ArgumentException("O preço do produto não pode ser abaixo de 0");
            }
        }
    }
}
