﻿using Estruturacao.Domain.Entidades;
using Estruturacao.Domain.EntitiesInOut.DTO.Categoria;
using Estruturacao.Domain.Interfaces.RegraNegocio;
using Estruturacao.Infra.Repositorio.Repositories.CategoriaRepository;

namespace Estruturacao.Application.RegraNegocio.RegraNegocioCategoria
{
    public class RegraNegocioCategoria : IRegraNegocioCategoria
    {
        private readonly ICategoriaRepository _categoriaRepository;
        public RegraNegocioCategoria(ICategoriaRepository categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        public Categoria Validar(int id, bool delete = false)
        {
            Categoria? categoria = _categoriaRepository.SelecionarPorId(id);

            if (categoria is null)
            {
                throw new NullReferenceException("Id Categoria Invalido");
            }

            return categoria;
        }

        public void Incluir(CategoriaDTO categoriaDTO)
        {
            if (categoriaDTO.Nome.Length > 50)
            {
                throw new ArgumentException("Nome da Categoria Muito Grande");
            }

            if (categoriaDTO.Nome.Length < 2)
            {
                throw new ArgumentException("Nome da Categoria Muito Curto");
            }

            Categoria categoria = new()
            {
                Nome = categoriaDTO.Nome,
                Modificacao = DateTime.Now,
            };

            _categoriaRepository.Adicionar(categoria);
        }
    }
}
